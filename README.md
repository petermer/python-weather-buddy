# Python Weather Buddy v0.0.1

A simple script to pull up weather data from OpenWeatherMap's API for the location entered. It 'conveniently' opens a Google Maps link to show you the detected city!