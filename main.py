import json
import urllib2
import webbrowser
from urllib import urlencode

city = raw_input('Enter city name: ')
args = {'units': 'metric',
        'q': city
        }
apiUrl = 'http://api.openweathermap.org/data/2.5/weather?'

data = json.load(urllib2.urlopen(apiUrl + urlencode(args)))

print 'Current temperature in %s is %d C.' % (data['name'], data['main']['temp'])

print 'Opening Google Maps for this location...'

mapsArgs = {'lat': data['coord']['lat'], 'lon': data['coord']['lon']}
mapsUrl = 'https://www.google.com/maps/preview/@'
mapsFinal = '%s%f,%f,12z' % (mapsUrl, mapsArgs['lat'], mapsArgs['lon'])

webbrowser.open(mapsFinal)
